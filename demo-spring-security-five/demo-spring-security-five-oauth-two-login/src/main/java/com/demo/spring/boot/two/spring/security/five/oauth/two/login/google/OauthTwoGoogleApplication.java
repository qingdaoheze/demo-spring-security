package com.demo.spring.boot.two.spring.security.five.oauth.two.login.google;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 2018/9/26
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@SpringBootApplication
public class OauthTwoGoogleApplication {
    public static void main(String[] args) {
        SpringApplication.run(OauthTwoGoogleApplication.class, args);
    }
}
