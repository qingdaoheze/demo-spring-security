package com.demo.spring.security.five.db.config;

import com.demo.spring.security.five.db.service.CustomUserService;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.SessionManagementConfigurer;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

/**
 * 2018/9/30
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private FilterInvocationSecurityMetadataSource securityMetadataSource;
    private MyAccessDecisionManager myAccessDecisionManager;

//    private MyFilterSecurityInterceptor myFilterSecurityInterceptor;
    private final CustomUserService customUserService;

    public WebSecurityConfig(CustomUserService customUserService, FilterInvocationSecurityMetadataSource securityMetadataSource,
                             MyAccessDecisionManager myAccessDecisionManager) {
        this.customUserService = customUserService;
        this.securityMetadataSource = securityMetadataSource;
        this.myAccessDecisionManager = myAccessDecisionManager;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .anyRequest().authenticated() //任何请求,登录后可以访问
                .and()
                .formLogin()
                .loginPage("/login")
                .failureUrl("/login?error")
                .permitAll() //登录页面用户任意访问
                .and()
                .logout().permitAll();
        MyFilterSecurityInterceptor myFilterSecurityInterceptor = new MyFilterSecurityInterceptor(securityMetadataSource, myAccessDecisionManager);
        http.addFilterAt(myFilterSecurityInterceptor, FilterSecurityInterceptor.class);
        // 删除指定过滤器
        http.removeConfigurer(SessionManagementConfigurer.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserService).passwordEncoder(NoOpPasswordEncoder.getInstance());
    }
}
