package com.demo.spring.security.five.db.dao;

import com.demo.spring.security.five.db.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * 2018/9/30
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
public interface UserMapper {
    SysUser findByUser(String username);
}
