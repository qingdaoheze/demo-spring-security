package com.demo.spring.security.five.db.service;

import com.demo.spring.security.five.db.dao.PermissionDao;
import com.demo.spring.security.five.db.dao.UserMapper;
import com.demo.spring.security.five.db.entity.Permission;
import com.demo.spring.security.five.db.entity.SysRole;
import com.demo.spring.security.five.db.entity.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 2018/9/30
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@Slf4j
@Service
public class CustomUserService implements UserDetailsService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PermissionDao permissionDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser user = userMapper.findByUser(username);
        if (user == null) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        List<GrantedAuthority> authorities = new ArrayList<>();
//        for (SysRole role : user.getRoles()) {
//            authorities.add(new SimpleGrantedAuthority(role.getName()));
//            log.info(role.getName());
//        }
        List<Permission> permissionList = permissionDao.findByAdminUserId(user.getId());
        for (Permission permission : permissionList) {
            if (permission != null && permission.getName() != null) {
                GrantedAuthority authority = new SimpleGrantedAuthority(permission.getName());
                authorities.add(authority);
            }
        }
        return new User(user.getUsername(), user.getPassword(), authorities);
    }
}
