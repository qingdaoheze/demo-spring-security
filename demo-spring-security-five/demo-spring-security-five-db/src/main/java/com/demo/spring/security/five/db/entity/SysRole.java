package com.demo.spring.security.five.db.entity;

/**
 * 2018/9/30
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
public class SysRole {
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
