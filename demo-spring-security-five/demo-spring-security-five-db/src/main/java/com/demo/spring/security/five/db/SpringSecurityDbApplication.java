package com.demo.spring.security.five.db;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 2018/9/30
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@SpringBootApplication
@MapperScan("com.demo.spring.security.five.db.dao")
public class SpringSecurityDbApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityDbApplication.class, args);
    }
}