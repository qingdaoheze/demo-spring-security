package com.demo.spring.security.five.db.dao;

import com.demo.spring.security.five.db.entity.Permission;

import java.util.List;

/**
 * 2018/9/30
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
public interface PermissionDao {
    public List<Permission> findAll();
    public List<Permission> findByAdminUserId(int userId);
}
