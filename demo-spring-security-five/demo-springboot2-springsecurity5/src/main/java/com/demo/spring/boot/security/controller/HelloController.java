package com.demo.spring.boot.security.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * 2018/9/12
 *
 * @author qd-hz
 */
@Controller
public class HelloController {
    private static final Logger logger = LoggerFactory.getLogger(HelloController.class);

    @SuppressWarnings("SameReturnValue")
    @RequestMapping("/")
    public String index(Model model) {
        getPrinciple(model);
        return "index";
    }

    @SuppressWarnings("SameReturnValue")
    @RequestMapping("/hello")
    public String hello(HttpServletResponse response, Model model) {
        getPrinciple(model);
        Cookie cookie = new Cookie("Hello", "world");
        response.addCookie(cookie);
        return "hello";
    }

    @SuppressWarnings("SameReturnValue")
    @RequestMapping("/login")
    public String login(Model model, @RequestParam(value = "logout", required = false) String logout) {
        getPrinciple(model);
        model.addAttribute("logout", logout == null ? "[null]" : "[" + logout + "]");
        // Param对象没有意义，只是用来验证idea对页面参数的校验
        return "login";
    }

    /** 不在security管理容器中，无法获取身份信息，即使用户已登录 */
    @RequestMapping("/filter/bypass")
    public String bypass(Model model) {
        getPrinciple(model);
        return "/filter/bypass";
    }

    /** 在security管理容器中，无需登录，可以获取身份信息。 */
    @RequestMapping("/filter/permitAll")
    public String permitAll(Model model) {
        getPrinciple(model);
        return "/filter/permitAll";
    }

    /** 在security管理容器中，无需登录，可以获取身份信息。只在无登录状态下才拥有这个身份。 */
    @RequestMapping("/filter/anonymous")
    public String anonymous(Model model) {
        getPrinciple(model);
        return "/filter/anonymous";
    }

    /** 在security管理容器中，必须登录，可以获取身份信息。 */
    @RequestMapping("/filter/authenticated")
    public String authenticated(Model model) {
        getPrinciple(model);
        return "/filter/authenticated";
    }

    /** 在security管理容器中，用户必须拥有admin角色 */
    @RequestMapping("/filter/admin")
    public String admin(Model model) {
        getPrinciple(model);
        return "/filter/admin";
    }

    /** 在security管理容器中，任何人都没有权限 */
    @RequestMapping("/filter/deny-all")
    public String noSecure(Model model) {
        getPrinciple(model);
        return "/filter/deny-all";
    }

    private void getPrinciple(Model model) {
        SecurityContext context = SecurityContextHolder.getContext();
        Object userName = null;
        if (context != null) {
            logger.debug("SecurityContext is not null.");
            Authentication authentication = context.getAuthentication();
            if (authentication != null) {
                logger.debug("Authentication is not null.");
                userName = authentication.getPrincipal() + "-" + authentication.getAuthorities();
            }
        }
        model.addAttribute("userName", userName);
    }

}
