package com.demo.spring.boot.security.controller;

/**
 * 2018/9/25
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
public class Param {
    private boolean error;
    private boolean logout;

    Param(boolean error, boolean logout) {
        this.error = error;
        this.logout = logout;
    }

    public boolean isError() {
        return error;
    }

    public boolean isLogout() {
        return logout;
    }

}