package com.demo.spring.boot.security.security.password.encoder;

/**
 * 2018/9/13
 *
 * @author qd-hz
 */
public class MyPasswordEncoder implements org.springframework.security.crypto.password.PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return s.equals(charSequence.toString());
    }
}
