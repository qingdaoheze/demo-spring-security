package com.demo.spring.boot.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 2018/9/12
 *
 * @author qd-hz
 */
@SpringBootApplication
public class SpringBootSecuritySampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootSecuritySampleApplication.class, args);
    }
}
