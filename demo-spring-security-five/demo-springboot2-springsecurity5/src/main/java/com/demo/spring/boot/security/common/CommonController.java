package com.demo.spring.boot.security.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 2018/9/29
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@Controller
@RequestMapping("common")
public class CommonController {
    @RequestMapping("noauth")
    public String noauth() {

        return "/common/noauth.html";
    }
}
