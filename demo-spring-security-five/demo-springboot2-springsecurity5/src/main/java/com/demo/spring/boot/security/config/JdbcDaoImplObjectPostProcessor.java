package com.demo.spring.boot.security.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.config.annotation.ObjectPostProcessor;

/**
 * 2018/9/29
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
public class JdbcDaoImplObjectPostProcessor implements ObjectPostProcessor<Object> {
    private static final Logger logger = LoggerFactory.getLogger(JdbcDaoImplObjectPostProcessor.class);
    @Override
    public <O> O postProcess(O object) {
        logger.warn(object.getClass().getName());
        return object;
    }
}
