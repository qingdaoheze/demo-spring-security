package com.demo.spring.boot.security.config;

import com.demo.spring.boot.security.security.password.encoder.MyPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

import javax.sql.DataSource;

/**
 * 2018/9/12
 *
 * @author qd-hz
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig  extends WebSecurityConfigurerAdapter {
    private final DataSource dataSource;

    public WebSecurityConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // 满足条件的请求才会受Spring Security管理
                .requestMatchers()
                    // 正则表达式路径配置。以/filter为前缀的地址忽略了/filter/bypass这个子路径
                    .regexMatchers("/filter/(?!bypass$)(?!bypass/)(?!bypass\\?).*",
                            "/login", "/login\\?.*", "/login/.*",
                            "/logout($|(\\?.*)|(/.*))")
                    // ant风格的路径配置
                    //.antMatchers("/filter/**", "/login", "/logout")
                .and()
                // 配置受管理请求需要的权限
                .authorizeRequests()
                    .antMatchers("/filter/permitAll").permitAll()
                    .antMatchers("/filter/anonymous").anonymous()
                    .mvcMatchers("/filter/admin").hasRole("admin")
                    .antMatchers("/filter/deny-all").denyAll()
                    .antMatchers("/", "/index").permitAll()
                    .anyRequest().authenticated()

                .and()
                    .exceptionHandling().accessDeniedPage("/common/noauth")
                .and()
                .formLogin()
                    .loginPage("/login")
                    .permitAll()

                .and()
                .logout()
                    // 注销成功后跳转到首页
                    // .logoutSuccessUrl("/")
                    .deleteCookies("Hello")
                    .permitAll()
                .and();
                // 只有UserDetailsService存在的时候有效
                // .rememberMe();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .passwordEncoder(new MyPasswordEncoder())
                .dataSource(dataSource)
                // 必须通过这个操作才能启用对group的支持
                .groupAuthoritiesByUsername(JdbcDaoImpl.DEF_GROUP_AUTHORITIES_BY_USERNAME_QUERY)
                .addObjectPostProcessor(new JdbcDaoImplObjectPostProcessor());
    }
}
