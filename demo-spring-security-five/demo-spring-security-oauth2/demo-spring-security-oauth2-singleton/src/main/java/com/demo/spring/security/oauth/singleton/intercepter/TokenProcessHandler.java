package com.demo.spring.security.oauth.singleton.intercepter;

import org.springframework.core.MethodParameter;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Map;
import java.util.Random;

/**
 * 2018/10/18
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
public class TokenProcessHandler implements HandlerMethodReturnValueHandler {
    @Override
    public boolean supportsReturnType(MethodParameter returnType) {

        return true;
    }

    @Override
    public void handleReturnValue(Object returnValue, MethodParameter returnType, ModelAndViewContainer mavContainer, NativeWebRequest webRequest) throws Exception {
        if (returnValue instanceof ResponseEntity) {
            ResponseEntity entity = (ResponseEntity) returnValue;
            Object body = entity.getBody();
            if (body != null) {
                if (body instanceof DefaultOAuth2AccessToken) {
                    DefaultOAuth2AccessToken accessToken = (DefaultOAuth2AccessToken)body;
                    Map<String, Object> additionalInformation = accessToken.getAdditionalInformation();
                    additionalInformation.put("ent1", new Random().nextInt(1000));
                }
            }
        }
    }
}
