package com.demo.spring.security.oauth.singleton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 2018/10/16
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@SpringBootApplication
public class SpringSecurityOauth2SingletonApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityOauth2SingletonApplication.class, args);
    }
}
