package com.demo.spring.security.oauth.singleton.intercepter;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Map;
import java.util.Random;

/**
 * 2018/10/18
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@ControllerAdvice
public class TokenResponseBodyAdvice implements ResponseBodyAdvice<DefaultOAuth2AccessToken> {
    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        if (!returnType.getMethod().getDeclaringClass().getName().equals(TokenEndpoint.class.getName())) {
            return false;
        }

        return true;
    }

    @Override
    public DefaultOAuth2AccessToken beforeBodyWrite(DefaultOAuth2AccessToken body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        DefaultOAuth2AccessToken accessToken = body;
        Map<String, Object> additionalInformation = accessToken.getAdditionalInformation();
        additionalInformation.put("ent", new Random().nextInt(1000));
        return body;
    }
}
