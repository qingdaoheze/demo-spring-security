package com.demo.spring.security.oauth.singleton.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 2018/10/18
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {
}
