package com.demo.spring.security.oauth2.seperate.resource.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

/**
 * 2018/10/24
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@Configuration
public class JwtTokenConfig {

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();

        jwtAccessTokenConverter.setSigningKey("MySigningKey");

        return jwtAccessTokenConverter;
    }
}
