package com.demo.spring.security.oauth2.seperate.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 2018/10/24
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@SpringBootApplication
public class ResourceServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ResourceServerApplication.class, args);
    }
}
