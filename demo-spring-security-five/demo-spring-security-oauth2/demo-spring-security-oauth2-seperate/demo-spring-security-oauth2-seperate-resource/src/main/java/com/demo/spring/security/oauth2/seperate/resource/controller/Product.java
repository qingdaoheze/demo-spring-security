package com.demo.spring.security.oauth2.seperate.resource.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 2018/10/24
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@RestController
public class Product {
    @RequestMapping("/product/{id1}")
    public String product(@PathVariable String id) {
        return String.format("Hello, the product id is %s.", id);
    }

    @RequestMapping("/order/{id}")
    public String order(@PathVariable String id) {
        return String.format("Hello, the order id is %s.", id);
    }
}
