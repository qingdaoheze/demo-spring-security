package com.demo.spring.security.oauth2.seperate.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.util.HashMap;
import java.util.Map;

/**
 * 2018/10/23
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {
    private final PasswordEncoder passwordEncoder;

    public AuthorizationServerConfiguration(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Value("${resource.id:spring-boot-application}")
    private String resourceId;

    @Value("${access_token.validity_period:3600}")
    private int accessTokenValiditySeconds = 3600;


    @Autowired
    private AuthenticationManager authenticationManager;


    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(authenticationManager);

        JwtAccessTokenConverter jwtAccessTokenConverter = jwtAccessTokenConverter();
        endpoints.accessTokenConverter(jwtAccessTokenConverter);
        endpoints.tokenStore(new JwtTokenStore(jwtAccessTokenConverter));
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        super.configure(security);
        security.passwordEncoder(passwordEncoder);
        security.allowFormAuthenticationForClients();
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        String encodedPassword = passwordEncoder.encode("123456");
        clients.withClientDetails(clientId -> {
            if (clientId != null) {
                BaseClientDetails clientDetails = null;
                switch (clientId) {
                    case "client1":
                        clientDetails = new BaseClientDetails(clientId, resourceId, "scope1,scope2",
                                "password, client_credentials", "client1_role1,client1_role2");
                        clientDetails.setClientSecret(encodedPassword);
                        clientDetails.setAccessTokenValiditySeconds(accessTokenValiditySeconds);
                        return clientDetails;
                    case "client2":
                        clientDetails = new BaseClientDetails(clientId, resourceId, "scope3,scope4",
                                "client_credentials", "client2_role1,client2_role2");
                        clientDetails.setClientSecret(encodedPassword);
                        return clientDetails;
                    default:
                        return null;
                }
            }

            return null;
        });
    }

    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter() {
            @Override
            public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
                String username = authentication.getUserAuthentication().getName();
                Map<String, Object> additionalInformation = new HashMap<>(16);
                additionalInformation.put("UserName", username);
                ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInformation);
                return super.enhance(accessToken, authentication);
            }
        };
        accessTokenConverter.setSigningKey("MySigningKey");
        return accessTokenConverter;
    }
}
