package com.demo.spring.security.five.impl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 2018/10/16
 * #
 * # Add some message
 * #
 *
 * @author qd-hz
 */
@SpringBootApplication
@MapperScan("com.demo.spring.security.five.impl.repository.mapper")
public class SpringSecurityImpl {
    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityImpl.class, args);
    }
}
