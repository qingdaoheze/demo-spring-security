**Spring Security 5.0.8.RELEASE笔记**


# 入门







## Maven方式引入

 **pom.xml.**  

```xml
<dependencies>
<!-- ... other dependency elements ... -->
<dependency>
	<groupId>org.springframework.security</groupId>
	<artifactId>spring-security-web</artifactId>
	<version>5.0.8.RELEASE</version>
</dependency>
<dependency>
	<groupId>org.springframework.security</groupId>
	<artifactId>spring-security-config</artifactId>
	<version>5.0.8.RELEASE</version>
</dependency>
</dependencies>
```

### snapshot and milestone

如果要使用snapshot或者是milestone版本则需要引入如下仓库。

 **pom.xml.**  

```xml
<repositories>
<!-- ... possibly other repository elements ... -->
    <repository>
        <!-- snapshot -->
        <id>spring-snapshot</id>
        <name>Spring Snapshot Repository</name>
        <url>http://repo.spring.io/snapshot</url>
    </repository>
    
    <repository>
        <!-- milestone -->
        <id>spring-milestone</id>
        <name>Spring Milestone Repository</name>
        <url>http://repo.spring.io/milestone</url>
    </repository>
</repositories>
```

### 依赖版本管理

 **pom.xml.**  

```xml
<dependencyManagement>
	<dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-framework-bom</artifactId>
            <version>5.0.9.RELEASE</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
	</dependencies>
</dependencyManagement>
```

## 模块

Spring Security根据功能领域细分为不同的模块，它们有各自独立的第三方依赖。

#### Core - spring-security-core.jar

核心组件，所有应用都必须使用的。

#### Web - spring-security-web.jar

web项目使用，包含了过滤器和有关网页安全的代码。

#### Config - spring-security-config.jar

XML配置和Java代码配置代码。

#### OAuth 2.0 Core - spring-security-oauth2-core.jar

Oauth服务端使用。

#### OAuth 2.0 Client - spring-security-oauth2-client.jar

Oauth2客户端使用。

#### OAuth 2.0 JOSE - spring-security-oauth2-jose.jar

包含了对JOSE(Javascript Object Signing and Encryption)的支持。

……

## 获取源码

```
git clone https://github.com/spring-projects/spring-security.git
```

# 例子和说明

| Source                                                       | Description                                                  | Guide                                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Hello Spring Security](https://github.com/spring-projects/spring-security/tree/5.0.8.RELEASE/samples/javaconfig/helloworld) | Demonstrates how to integrate Spring Security with an existing application using Java-based configuration. | [Hello Spring Security Guide](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/guides/html5/helloworld-javaconfig.html) |
| [Hello Spring Security Boot](https://github.com/spring-projects/spring-security/tree/5.0.8.RELEASE/samples/boot/helloworld) | Demonstrates how to integrate Spring Security with an existing Spring Boot application. | [Hello Spring Security Boot Guide](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/guides/html5/helloworld-boot.html) |
| [Hello Spring Security XML](https://github.com/spring-projects/spring-security/tree/5.0.8.RELEASE/samples/xml/helloworld) | Demonstrates how to integrate Spring Security with an existing application using XML-based configuration. | [Hello Spring Security XML Guide](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/guides/html5/helloworld-xml.html) |
| [Hello Spring MVC Security](https://github.com/spring-projects/spring-security/tree/5.0.8.RELEASE/samples/javaconfig/hellomvc) | Demonstrates how to integrate Spring Security with an existing Spring MVC application. | [Hello Spring MVC Security Guide](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/guides/html5/hellomvc-javaconfig.html) |
| [Custom Login Form](https://github.com/spring-projects/spring-security/tree/5.0.8.RELEASE/samples/javaconfig/form) | Demonstrates how to create a custom login form.              | [Custom Login Form Guide](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/guides/html5/form-javaconfig.html) |
| [OAuth 2.0 Login](https://github.com/spring-projects/spring-security/tree/5.0.8.RELEASE/samples/boot/oauth2login) | Demonstrates how to integrate OAuth 2.0 Login with an OAuth 2.0 or OpenID Connect 1.0 Provider. | [OAuth 2.0 Login Guide](https://github.com/spring-projects/spring-security/tree/5.0.8.RELEASE/samples/boot/oauth2login/README.adoc) |



# 代码配置

## 初识

```java
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.authentication.builders.*;
import org.springframework.security.config.annotation.web.configuration.*;

@EnableWebSecurity
public class WebSecurityConfig implements WebMvcConfigurer {

	@Bean
	public UserDetailsService userDetailsService() throws Exception {
		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
		manager.createUser(User.withDefaultPasswordEncoder().username("user").password("password").roles("USER").build());
		return manager;
	}
}
```

这个配置中的，@EnableWebSecurity将创建一个过滤器，用于拦截所有的请求。但这时它并没有注册到web容器中，需要后面的步骤来完成注册。另外，它还完成了其他很多任务。

### 非Spring项目

通过如下代码将上面的WebSecurityConfig注册到容器中。

```java
import org.springframework.security.web.context.*;

public class SecurityWebApplicationInitializer
	extends AbstractSecurityWebApplicationInitializer {

	public SecurityWebApplicationInitializer() {
        // 由其超类负责注册
		super(WebSecurityConfig.class);
	}
}
```

### Spring MVC项目

项目中已经有了`WebApplicationInitializer`，它用来加载spring配置。直接使用之前的配置会报错。我们需要将WebSecurityConfig注册到已经存在的ApplicationContext中。

Spring MVC配置：

```java
import org.springframework.security.web.context.*;

public class SecurityWebApplicationInitializer
	extends AbstractSecurityWebApplicationInitializer {

}
```

Spring Security配置

```java
public class MvcWebApplicationInitializer extends
		AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { WebSecurityConfig.class };
	}

	// ... other overrides ...
}
```

## HttpSecurity

前面的[WebSecurityConfig](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/reference/htmlsingle/#jc-hello-wsca)只包含了用户认证的信息。如何让Spring Security知道我们要所有用户通过认证？如何让Spring Security知道我们支持基于form的认证？原因是`WebSecurityConfigurerAdapter`在方法`configure(HttpSecurity http)`中提供了默认配置，如下：

```java
protected void configure(HttpSecurity http) throws Exception {
	http
		.authorizeRequests()
			.anyRequest().authenticated()// 拦截所有请求，要求所有用户被验证
			.and()
		.formLogin() // 支持form认证
			.and()
		.httpBasic(); // 支持BASIC认证
}
```

它跟XML配置非常相似。

```xml
<http>
	<intercept-url pattern="/**" access="authenticated"/>
	<form-login />
	<http-basic />
</http>
```

关闭XML标签的java配置等价物为`and()`，它是我们可以继续配置父对象。

**注意：**Java配置的默认URL和参数是不一样的。

## 禁用默认过滤器

只需要在扩展`WebSecurityConfigurerAdapter`时，显示调用其构造器方法`WebSecurityConfigurerAdapter(true)`。

## 配置form登录

Spring Security基于启用的特性自动生成了一个登录页面，它将处理提交的请求，并将用户重定向到目标URL。

要提供自定义的登录页面，这样配置：

```java
protected void configure(HttpSecurity http) throws Exception {
	http
		.authorizeRequests()
			.anyRequest().authenticated()
			.and()
		.formLogin()
			.loginPage("/login") // 登录地址
			.permitAll();        // 所有用户可以访问此地址
}
```

JSP页面的例子如下：

```JSP
<c:url value="/login" var="loginUrl"/>
<form action="${loginUrl}" method="post"><!-- post请求将尝试认证用户 -->      
	<c:if test="${param.error != null}"><!-- 尝试认证后失败了 -->
		<p>
			Invalid username and password.
		</p>
	</c:if>
	<c:if test="${param.logout != null}"><!-- 注销登录了 -->
		<p>
			You have been logged out.
		</p>
	</c:if>
	<p>
		<label for="username">Username</label>
		<input type="text" id="username" name="username"/><!-- 用户名 -->
	</p>
	<p>
		<label for="password">Password</label>
		<input type="password" id="password" name="password"/><!-- 密码 -->
	</p>
	<input type="hidden"                        
		name="${_csrf.parameterName}" <!-- Cross Site Request Forgery (CSRF)  -->
		value="${_csrf.token}"/>
	<button type="submit" class="btn">Log in</button>
</form>
```

##  授权请求

添加多个子元素到`http.authorizeRequests()`来定制不同URL的授权。

```java
protected void configure(HttpSecurity http) throws Exception {
	http
		.authorizeRequests()
        	// 每个地址按照声明顺序进行匹配
        	// ant风格的匹配串，满足的路径不需要认证
			.antMatchers("/resources/**", "/signup", "/about").permitAll()
        	// 拥有ADMIN角色的用户可以访问/admin下的内容
			.antMatchers("/admin/**").hasRole("ADMIN")  
        	// 拥有ADMIN和DBA两个角色的用户可以访问/db下的内容
			.antMatchers("/db/**").access("hasRole('ADMIN') and hasRole('DBA')") 
        	// 其他请求都需要用户认证
			.anyRequest().authenticated()
			.and()
		// ...
		.formLogin();
}
```

## 处理注销

使用`WebSecurityConfigurerAdapter`时，logout能力是自动应用的。默认情况，访问/logout将注销用户。注销过程包含：

* 使HTTP会话失效
* 清除配置的RememberMe认证信息
* 清除`SecurityContextHolder`
* 重定向到`/login?logout`

定制注销：

```java
protected void configure(HttpSecurity http) throws Exception {
	http
		.logout()                                                                
			.logoutUrl("/my/logout") // 触发注销的url。CSRF防护启用时，必须为POST方法
			.logoutSuccessUrl("/my/index") // 注销成功后跳转的地址
			.logoutSuccessHandler(logoutSuccessHandler) // 注销成功处理器。将使logoutSuccessUrl被忽略。
			.invalidateHttpSession(true) // 使会话失效
			.addLogoutHandler(logoutHandler) // 注销处理器。默认，SecurityContextLogoutHandler被添加为最后一个handler。
			.deleteCookies(cookieNamesToClear) // 删除Cookie。添加CookieClearingLogoutHandler的快捷方式
			.and()
		...
}
```

### LogoutHandler

通常[LogoutHandler](https://docs.spring.io/spring-security/site/docs/current/apidocs/org/springframework/security/web/authentication/logout/LogoutHandler.html)实现指定能够参与注销处理的类。它们不能抛出异常。提供的实现有：

-  [PersistentTokenBasedRememberMeServices](https://docs.spring.io/spring-security/site/docs/current/apidocs/org/springframework/security/web/authentication/rememberme/PersistentTokenBasedRememberMeServices.html) 
-  [TokenBasedRememberMeServices](https://docs.spring.io/spring-security/site/docs/current/apidocs/org/springframework/security/web/authentication/rememberme/TokenBasedRememberMeServices.html) 
-  [CookieClearingLogoutHandler](https://docs.spring.io/spring-security/site/docs/current/apidocs/org/springframework/security/web/authentication/logout/CookieClearingLogoutHandler.html) 
-  [CsrfLogoutHandler](https://docs.spring.io/spring-security/site/docs/current/apidocs/org/springframework/security/web/csrf/CsrfLogoutHandler.html) 
-  [SecurityContextLogoutHandler](https://docs.spring.io/spring-security/site/docs/current/apidocs/org/springframework/security/web/authentication/logout/SecurityContextLogoutHandler.html) 

流式API提供了快捷方式在底层提供各自的`LogoutHandler`实现，可用于替代明确指定。例如`deleteCookies()`用于在注销成功后删除指定的Cookie。

### LogoutSuccessHandler

`LogoutSuccessHandler`在注销成功后，由`LogoutFilter`调用，用于重定向或者转发到合适的目的地。它跟`LogoutHandler`非常相似，但可以抛出异常。

-  [SimpleUrlLogoutSuccessHandler](https://docs.spring.io/spring-security/site/docs/current/apidocs/org/springframework/security/web/authentication/logout/SimpleUrlLogoutSuccessHandler.html) 
-  HttpStatusReturningLogoutSuccessHandler 

`logoutSuccessUrl()`是`SimpleUrlLogoutSuccessHandler`的快捷方式。

`HttpStatusReturningLogoutSuccessHandler`在REST API模式下可能是有用的，它使我们可以提供一个特殊的http响应码，而不是在注销成功后，重定向到一个别的地址。

## OAuth 2.0 Login

这个特性带给应用一种能力，即让用户使用他们在OAuth 2.0 Provider（如github）或者OpenID Connect 1.0 Provider（如Google）中的存在的账号登录这个应用。

`ClientRegistration`代表在OAuth provider中注册的一个客户端。

`ClientRegistrationRepository`代表了`ClientRegistration`的一个仓库。

### 默认Provider

`org.springframework.security.config.oauth2.client.CommonOAuth2Provider`默认添加了4种OAuth2 Provider，它们是GOOGLE，GITHUB，FACEBOOK，OKTA。

因此在配置时，只需要配置较少的参数，如google（`client-id` 和 `client-secret`）：

```yaml
spring:
  security:
    oauth2:
      client:
        registration:
          google:
            client-id: google-client-id
            client-secret: google-client-secret
```

在这里google是`registrationId`，它与CommonOAuth2Provider中的枚举GOOGLE匹配（不区分大小写），因此可以使用对应的默认属性。

如果要为google指定一个别的`registrationId`，也可以利用默认属性，只需要配置`provider`属性，如下：

```yaml
spring:
  security:
    oauth2:
      client:
        registration:
          google-login:	
            provider: google	
            client-id: google-client-id
            client-secret: google-client-secret
```

### 覆盖Spring Boot的自动配置

自动配置类为`OAuth2ClientAutoConfiguration`。它做了如下任务：

* 将oauth client属性配置的`ClientRegistration`（代表一个OAuth服务方）组成的`ClientRegistrationRepository`注册为Bean。

* 提供了`WebSecurityConfigurerAdapter`配置，并通过`httpSecurity.oauth2Login()`

  启用了OAuth2登录。

#### Register a `ClientRegistrationRepository`Bean

```java
@Configuration
public class OAuth2LoginConfig {

	@Bean
	public ClientRegistrationRepository clientRegistrationRepository() {
		return new InMemoryClientRegistrationRepository(this.googleClientRegistration());
	}

	private ClientRegistration googleClientRegistration() {
		return ClientRegistration.withRegistrationId("google")
			.clientId("google-client-id")
			.clientSecret("google-client-secret")
			.clientAuthenticationMethod(ClientAuthenticationMethod.BASIC)
			.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
			.redirectUriTemplate("{baseUrl}/login/oauth2/code/{registrationId}")
			.scope("openid", "profile", "email", "address", "phone")
			.authorizationUri("https://accounts.google.com/o/oauth2/v2/auth")
			.tokenUri("https://www.googleapis.com/oauth2/v4/token")
			.userInfoUri("https://www.googleapis.com/oauth2/v3/userinfo")
			.userNameAttributeName(IdTokenClaimNames.SUB)
			.jwkSetUri("https://www.googleapis.com/oauth2/v3/certs")
			.clientName("Google")
			.build();
	}
}
```

#### Provide a `WebSecurityConfigurerAdapter`

```java
@EnableWebSecurity
public class OAuth2LoginSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.anyRequest().authenticated()
				.and()
			.oauth2Login();
	}
}
```

#### Completely Override the Auto-configuration

即上面2种方式的结合，可以完全覆盖自动配置

```java
@Configuration
public class OAuth2LoginConfig {

	@EnableWebSecurity
	public static class OAuth2LoginSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http
				.authorizeRequests()
					.anyRequest().authenticated()
					.and()
				.oauth2Login();
		}
	}

	@Bean
	public ClientRegistrationRepository clientRegistrationRepository() {
		return new InMemoryClientRegistrationRepository(this.googleClientRegistration());
	}

	private ClientRegistration googleClientRegistration() {
		return ClientRegistration.withRegistrationId("google")
			.clientId("google-client-id")
			.clientSecret("google-client-secret")
			.clientAuthenticationMethod(ClientAuthenticationMethod.BASIC)
			.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
			.redirectUriTemplate("{baseUrl}/login/oauth2/code/{registrationId}")
			.scope("openid", "profile", "email", "address", "phone")
			.authorizationUri("https://accounts.google.com/o/oauth2/v2/auth")
			.tokenUri("https://www.googleapis.com/oauth2/v4/token")
			.userInfoUri("https://www.googleapis.com/oauth2/v3/userinfo")
			.userNameAttributeName(IdTokenClaimNames.SUB)
			.jwkSetUri("https://www.googleapis.com/oauth2/v3/certs")
			.clientName("Google")
			.build();
	}
}
```

### `OAuth2AuthorizedClient`和`OAuth2AuthorizedClientService`

`OAuth2AuthorizedClient`代表授权客户端。终端用户（资源拥有者）将访问其受保护资源的权利授予一个客户端，即认为这个客户端是被授权的。

`OAuth2AuthorizedClient`有助于将`OAuth2AccessToken`与`ClientRegistration`和资源拥有者联系起来。资源拥有者是授予权利的终端用户。

`OAuth2AuthorizedClientService`的主要角色是管理`OAuth2AuthorizedClient`实例。从开发者的角度讲，它提供了查找和客户端关联的`OAuth2AccessToken`的能力，以便客户端可以使用它初始化到资源服务器的请求。

> Spring Boot自动配置在Application Context中注册一个`OAuth2AuthorizedClientService`Bean。

开发者可以注入`OAuth2AuthorizedClientService`以获取它的能力。

```java
@Controller
public class MainController {

	@Autowired
	private OAuth2AuthorizedClientService authorizedClientService;

	@RequestMapping("/userinfo")
	public String userinfo(OAuth2AuthenticationToken authentication) {
		// authentication.getAuthorizedClientRegistrationId() returns the
		// registrationId of the Client that was authorized during the Login flow
		OAuth2AuthorizedClient authorizedClient =
			this.authorizedClientService.loadAuthorizedClient(
				authentication.getAuthorizedClientRegistrationId(),
				authentication.getName());

		OAuth2AccessToken accessToken = authorizedClient.getAccessToken();

		...

		return "userinfo";
	}
}
```

### 其他资源

如下资源描述了高级配置选项。

-  [OAuth 2.0 Login Page](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/reference/htmlsingle/#oauth2login-advanced-login-page) 
- Authorization Endpoint:
  -  [AuthorizationRequestRepository](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/reference/htmlsingle/#oauth2login-advanced-authorization-request-repository) 
-  [Redirection Endpoint](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/reference/htmlsingle/#oauth2login-advanced-redirection-endpoint) 
- Token Endpoint:
  -  [OAuth2AccessTokenResponseClient](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/reference/htmlsingle/#oauth2login-advanced-token-client) 
- UserInfo Endpoint:
  -  [Mapping User Authorities](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/reference/htmlsingle/#oauth2login-advanced-map-authorities) 
  -  [Configuring a Custom OAuth2User](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/reference/htmlsingle/#oauth2login-advanced-custom-user) 
  -  [OAuth 2.0 UserService](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/reference/htmlsingle/#oauth2login-advanced-oauth2-user-service) 
  -  [OpenID Connect 1.0 UserService](https://docs.spring.io/spring-security/site/docs/5.0.8.RELEASE/reference/htmlsingle/#oauth2login-advanced-oidc-user-service) 

## 认证

配置认证的高级一点的内容

### In-Memory Authentication

```java
@Bean
public UserDetailsService userDetailsService() throws Exception {
	// ensure the passwords are encoded properly
	UserBuilder users = User.withDefaultPasswordEncoder();
	InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
	manager.createUser(users.username("user").password("password").roles("USER").build());
    manager.createUser(users.username("admin").password("password").roles("USER","ADMIN").build());
	return manager;
}
```

### JDBC Authentication

[官方例子](https://github.com/spring-projects/spring-security/blob/master/samples/javaconfig/jdbc/spring-security-samples-javaconfig-jdbc.gradle)

```java
@Autowired
private DataSource dataSource;

@Autowired
public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
	// ensure the passwords are encoded properly
	UserBuilder users = User.withDefaultPasswordEncoder();
	auth
		.jdbcAuthentication()
			.dataSource(dataSource)
			.withDefaultSchema()
			.withUser(users.username("user").password("password").roles("USER"))
			.withUser(users.username("admin").password("password").roles("USER","ADMIN"));
}
```

### AuthenticationProvider

可以注册一个`AuthenticationProvider`Bean来定义自定义认证。

```java
// SpringAuthenticationProvider实现了AuthenticationProvider接口
@Bean
public SpringAuthenticationProvider springAuthenticationProvider() {
	return new SpringAuthenticationProvider();
}
```

> **注意：**只有在AuthenticationManagerBuilder没有被填充的时候有效

### UserDetailsService

可以注册一个`UserDetailsService`Bean来定义自定义认证。

```java
// SpringDataUserDetailsService实现了接口UserDetailsService
@Bean
public SpringDataUserDetailsService springDataUserDetailsService() {
	return new SpringDataUserDetailsService();
}
```

>  **注意：**只有在`AuthenticationManagerBuilder`没有被填充，且没有`AuthenticationProviderBean`定义的情况下有用。

## 多个HttpSecurity

正如我们可以使用多个`<http>`块一样，我们可以配置多个HttpSecurity实例。关键是扩展`WebSecurityConfigurationAdapter`多次。

```java
@EnableWebSecurity
public class MultiHttpSecurityConfig {
	@Bean                                                             
	public UserDetailsService userDetailsService() throws Exception {
		// ensure the passwords are encoded properly
		UserBuilder users = User.withDefaultPasswordEncoder();
		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
		manager.createUser(users.username("user").password("password").roles("USER").build());
		manager.createUser(users.username("admin").password("password").roles("USER","ADMIN").build());
		return manager;
	}

    // 第一次扩展。 Order指定了配置顺序，不指定则为最后。1优先级最高，数值越大，优先级越低。
    // /api开始的请求会被这个配置匹配，其他的才会使用下面的扩展。
	@Configuration
	@Order(1)                                                        
	public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
		protected void configure(HttpSecurity http) throws Exception {
			http
				.antMatcher("/api/**")                               
				.authorizeRequests()
					.anyRequest().hasRole("ADMIN")
					.and()
				.httpBasic();
		}
	}

    // 第二次扩展
	@Configuration                                                   
	public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http
				.authorizeRequests()
					.anyRequest().authenticated()
					.and()
				.formLogin();
		}
	}
}
```

## Method Security

### EnableGlobalMethodSecurity

可以在`@Configuration`实例上使用`@EnableGlobalMethodSecurity`启用基于注解的安全。例如，如下代码将启用Spring Security的`@Secured`注解。

```java
@EnableGlobalMethodSecurity(securedEnabled = true)
public class MethodSecurityConfig {
// ...
}
```

添加注解到方法（类或者接口里的方法都可以）将限制对该方法的访问。Spring Security自有注解支持一套属性。它们可以被传递给AccessDecisionManager，供它做决策。

```java
public interface BankService {

@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
public Account readAccount(Long id);

@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
public Account[] findAccounts();

@Secured("ROLE_TELLER")
public Account post(Account account, double amount);
}
```

启用对JSR-250注解的支持，需要配置jsr250Enabled = true：

```java
@EnableGlobalMethodSecurity(jsr250Enabled = true)
public class MethodSecurityConfig {
// ...
}
```

这些基于标准的注解允许应用简单的基于角色的约束，没有Spring Security自有的注解强大。

启用基于表达式的语法，需要如下配置prePostEnabled = true：

```java
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MethodSecurityConfig {
// ...
}
```

上面例子的等价配置为

```java
public interface BankService {

@PreAuthorize("isAnonymous()")
public Account readAccount(Long id);

@PreAuthorize("isAnonymous()")
public Account[] findAccounts();

@PreAuthorize("hasAuthority('ROLE_TELLER')")
public Account post(Account account, double amount);
}
```

### GlobalMethodSecurityConfiguration

有时，我们需要执行一些比`@EnableGlobalMethodSecurity`允许的更复杂的操作。对于这些情况，可以扩展`GlobalMethodSecurityConfiguration`，并且确保`@EnableGlobalMethodSecurity`添加到该实现类上。

例如，要提供一个定制的`MethodSecurityExpressionHandler`，可以使用如下配置：

```java
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {
	@Override
	protected MethodSecurityExpressionHandler createExpressionHandler() {
		// ... create and return custom MethodSecurityExpressionHandler ...
		return expressionHandler;
	}
}
```

## 后置处理配置对象

Spring Security的java配置没有把每个配置对象的所有属性都暴露出来，是为了简化大部分人的配置。毕竟，如果每个属性都暴露出来，用户需要使用标准的bean配置每一个属性。

尽管有很好的理由不直接暴露所有的属性，用户可能仍然需要更多的高级配置选项。为了解决这个问题，Spring Security引入了`ObjectPostProcessor`，它可用于修改或替换java配置创建的对象实例。例如，如果想修改`FilterSecurityInterceptor`的`filterSecurityPublishAuthorizationSuccess`属性，可以使用下面的代码：

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
	http
		.authorizeRequests()
			.anyRequest().authenticated()
			.withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
				public <O extends FilterSecurityInterceptor> O postProcess(
						O fsi) {
					fsi.setPublishAuthorizationSuccess(true);
					return fsi;
				}
			});
}
```

## 自定义DSL

在Spring Security中，可以自定义自己的DSL。例如，我们定义DSL如下：

```java
public class MyCustomDsl extends AbstractHttpConfigurer<MyCustomDsl, HttpSecurity> {
	private boolean flag;

	@Override
	public void init(H http) throws Exception {
		// any method that adds another configurer
		// must be done in the init method
		http.csrf().disable();
	}

	@Override
	public void configure(H http) throws Exception {
		ApplicationContext context = http.getSharedObject(ApplicationContext.class);

		// here we lookup from the ApplicationContext. You can also just create a new instance.
		MyFilter myFilter = context.getBean(MyFilter.class);
		myFilter.setFlag(flag);
		http.addFilterBefore(myFilter, UsernamePasswordAuthenticationFilter.class);
	}

	public MyCustomDsl flag(boolean value) {
		this.flag = value;
		return this;
	}

	public static MyCustomDsl customDsl() {
		return new MyCustomDsl();
	}
}
```

> 这段代码实际上展示了类似`HttpSecurity.authorizeRequests()`的方法是如何实现的。

然后这样使用自定义的DSL：

```java
@EnableWebSecurity
public class Config extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.apply(customDsl())
				.flag(true)
				.and()
			...;
	}
}
```

代码的调用顺序如下：

-  Code in `Config`s configure method is invoked 
-  Code in `MyCustomDsl`s init method is invoked 
-  Code in `MyCustomDsl`s configure method is invoked 

也可以使用`SpringFactories`让`WebSecurityConfiguerAdapter`默认添加`MyCustomDsl`。只需要在类路径下添加文件`META-INF/spring.factories`，并添加如下内容：

**META-INF/spring.factories.**

```properties
org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer = sample.MyCustomDsl
```

然后如果有人想禁用这个默认配置，可以这样明确的操作：

```java
@EnableWebSecurity
public class Config extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.apply(customDsl()).disable()
			...;
	}
}
```

# XML配置

## 序言

增加了security命名空间，使用这个命名空间可以对原有命名空间提供很好的补充，而且也通过这个名称空间引入了一些简化的配置。更好的提高了配置效率。

## security命名空间配置入门

### web.xml配置

第一步是web.xml中添加过滤器，如下：

```xml
<filter>
<filter-name>springSecurityFilterChain</filter-name>
<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
</filter>

<filter-mapping>
<filter-name>springSecurityFilterChain</filter-name>
<url-pattern>/*</url-pattern>
</filter-mapping>
```

这段配置在Spring Security互联网基础设施中添加了一个挂钩。`DelegatingFilterProxy`将代理到Spring Context中的一个实现了Filter接口名称为`springSecurityFilterChain`的bean中。security命名空间的加入会自动创建这个bean。

### 最简单的<http>配置

作为起步，启用web安全需要做的全部内容就是如下配置：

```xml
<http>
    <!--
	拦截所有请求，只有拥有ROLE_USER角色的用户可以访问。
	pattern指定了一个ant风格的匹配策略，满足策略的请求地址才被允许
 	-->
    <intercept-url pattern="/**" access="hasRole('USER')" />
    <!-- 使用form方式登录 -->
    <form-login />
    <!-- 注册一个注销地址 -->
    <logout />
</http>
```

`ROLE_`前缀是一个标记，它指出与用户的授权做一个简单的比较。access属性可以指定以逗号分隔的多个值。

> `<intercept-url>`可以指定多个，它们会按顺序依次计算第一个匹配的将被使用。因此高优先级的要放在上面。还可以通过`method`指定明确的HTTP方法。

添加用户

```xml
<authentication-manager>
    <authentication-provider>
        <user-service>
        <!-- Password is prefixed with {noop} to indicate to DelegatingPasswordEncoder that
        NoOpPasswordEncoder should be used. This is not safe for production, but makes reading
        in samples easier. Normally passwords should be hashed using BCrypt -->
        <user name="jimi" password="{noop}jimispassword" authorities="ROLE_USER, ROLE_ADMIN" />
        <user name="bob" password="{noop}bobspassword" authorities="ROLE_USER" />
        </user-service>
    </authentication-provider>
</authentication-manager>
```

`password`属性中的前缀用于指示DelegatingPasswordEncoder使用对应的密码编码器，例如`{bcrypt}`对应`BCryptPasswordEncoder`。

`<authentication-provider>`创建了`DaoAuthenticationProvider`bean，`<user-service>`创建了`InMemoryDaoImpl`bean。所有的`authentication-provider`都必须是`<authentication-manager>`子元素。`<authentication-manager>`创建了一个`ProviderManager`bean。

可以将用户和密码信息配置到外部的属性文件中，如：

```xml
<user-service id="userDetailsService" properties="users.properties"/>
```

属性文件中条目的格式为：

```properties
username=password,grantedAuthority[,grantedAuthority][,enabled|disabled]
```

例如：

```properties
jimi=jimispassword,ROLE_USER,ROLE_ADMIN,enabled
bob=bobspassword,ROLE_USER,enabled
```

### Form和Basic认证

默认，Spring Security自动为form认证生成了录入页面和登录行为。可以自定义登录页面

```xml
<http>
    <!-- 将登录页面排除在要求登录的列表之外 -->
    <intercept-url pattern="/login.jsp*" access="IS_AUTHENTICATED_ANONYMOUSLY"/>
    <intercept-url pattern="/**" access="ROLE_USER" />
    <!-- login-page指定登录页面 -->
    <form-login login-page='/login.jsp'/>
</http>
```

 也可以使用http元素完全绕过安全过滤器

```xml
<http pattern="/css/**" security="none"/>
<http pattern="/login.jsp*" security="none"/>

<http use-expressions="false">
    <intercept-url pattern="/**" access="ROLE_USER" />
    <form-login login-page='/login.jsp'/>
</http>
```

<http pattern="/css/**" security="none"/>这样定义，满足条件的请求将不经过安全过滤器链，即完全无视spring security，因此，请求过程中将无法访问用户信息或者受保护的方法。如果要让这些请求经过安全过滤器，可以在这个元素上添加属性`access='IS_AUTHENTICATED_ANONYMOUSLY'`。

#### 设置默认登录成功跳转页面

在直接进入登录页面的情况下（不是因为访问受保护资源被重定向的），可以通过`default-target-url`来定义登录成功后目标页面。也可以使用`always-use-default-target`来让所有情况下的登录成功都跳转到默认结果页面。

要对目标页面进行更多的控制，可以使用`authentication-success-handler-ref`来代替`default-target-url`。这个属性的值指定一个`AuthenticationSuccessHandler`类型的bean。

## 高级Web特性

### Remember-Me认证



### 添加HTTP/HTTPS频道保护

`<intercept-url>`的`requires-channel`属性可以强制一个请求只能使用某种协议。

```xml
<http>
    <intercept-url pattern="/secure/**" access="ROLE_USER" requires-channel="https"/>
    <intercept-url pattern="/**" access="ROLE_USER" requires-channel="any"/>
    ...
</http>
```

如果请求的协议不是要求的协议，请求首先会被重定向到要求的协议。可以按照如下方式定义不同协议对应的端口。

```xml
<http>
    ...
    <port-mappings>
        <port-mapping http="9080" https="9443"/>
    </port-mappings>
</http>
```

### 会话管理

#### 检测超时

可以通过`session-management`来让Spring Security检测无效的会话，并重定向用户到适合的URL。

```xml
<http>
    ...
    <session-management invalid-session-url="/invalidSession.htm" />
</http>
```

注意，使用这种机制时，如果用户注销后，没有关闭浏览器，再重新登录，则会报一个错误。这是因为注销登录时，会话cookie没有被清除，又被重新提交回来了。可以使用如下方法，在注销的时候清除JSESSIONID这个cookie。

```xml
<http>
	<logout delete-cookies="JSESSIONID" />
</http>
```

但这种方法不能保证在所有的容器中工作，因此需要在对应的环境上测试。

#### 会话并发控制

如果要约束单个账号只能有一个会话，可以如下操作：

1. 添加监听器，使Spring Security能够感知会话生命周期事件。

   ```xml
   <listener>
       <listener-class>
           org.springframework.security.web.session.HttpSessionEventPublisher
       </listener-class>
   </listener>
   ```

2. 定义并发数量

   ```xml
   <http>
       ...
       <session-management>
           <concurrency-control max-sessions="1" />
       </session-management>
   </http>
   ```

这样配置，同一个用户第二次登录会使前一次登录失效。也可以通过如下配置，使第二次登录失败。

```xml
<http>
    ...
    <session-management>
        <concurrency-control max-sessions="1" error-if-maximum-exceeded="true" />
    </session-management>
</http>
```

如果使用的基于form的登录，用户会被重定向到`authentication-failure-url`。如果是非交互机制，例如Remember-Me，将返回一个unauthorized（401）错误。如果要为其指定一个专有的错误页面，可以使用`session-management`的`session-authentication-error-url`。

#### 会话固定攻击防护

使用`<session-management>`的`session-fixation-protection`属性可以指定采取什么策略。有4种可选的策略。

-  `none` - Don’t do anything. The original session will be retained. 
-  `newSession` - Create a new "clean" session,  without copying the existing session data (Spring Security-related  attributes will still be copied). 
-  `migrateSession` - Create a new session and  copy all existing session attributes to the new session. This is the  default in Servlet 3.0 or older containers. 
-  `changeSessionId` - Do not create a new session. Instead, use the session fixation protection provided by the Servlet container (`HttpServletRequest#changeSessionId()`).  This option is only available in Servlet 3.1 (Java EE 7) and newer  containers. Specifying it in older containers will result in an  exception. This is the default in Servlet 3.1 and newer containers. 

当会话固定防护发生时，会导致一个`SessionFixationProtectionEvent`发布到应用上下文中。如果采用了`changeSessionId`策略，会导致`javax.servlet.http.HttpSessionIdListener`被通知。

### OpenID支持

security命名空间还支持OpenID登录。

```xml
<http>
    <intercept-url pattern="/**" access="ROLE_USER" />
    <openid-login />
</http>
```

然后，需要将应用本身注册到OpenID Provider（例如：myopenid.com）上，并且添加用户信息到内存`<user-service>`。

```xml
<user name="http://jimi.hendrix.myopenid.com/" authorities="ROLE_USER" />
```

这样就可以在myopenid.com上认证。

#### 属性交换

支持OpenID属性交换。如下配置将尝试从OpenID provider中获取email和全名称，供应用使用。

```xml
<openid-login>
    <attribute-exchange>
        <openid-attribute name="email" type="http://axschema.org/contact/email" required="true"/>
        <openid-attribute name="name" type="http://axschema.org/namePerson"/>
    </attribute-exchange>
</openid-login>
```

type属性是一个URI，由一个具体的方案确定，在本例中是http://axschema.org/。如果在认证成功后，一个属性必须被获取，可以设置`required`属性。具体支持什么方案和属性，依赖于OpenID Provider。作为认证过程的一部分，这些属性的值被返回，之后，可以使用如下代码来访问。

```java
OpenIDAuthenticationToken token =
	(OpenIDAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
List<OpenIDAttribute> attributes = token.getAttributes();
```

`OpenIDAttribute`包含了属性类型和获取的值（在多值属性是多个值）。如果我们希望使用多个身份提供者，多个属性交换配置也是支持的。可以提供多个`attribute-exchange`元素，每个都使用`identifier-matcher`属性。这个属性包含了一个正则表达式，它依靠用户提供的OpenID标识符来匹配。可以查看例子程序：<https://github.com/spring-projects/spring-security/tree/5.0.8.RELEASE/samples/boot/oauth2login>

### 添加自己的过滤器

默认提供的标准过滤器有（按照它们在过滤器链中的顺序排列）：

| Alias                        | Filter Class                                          | Namespace Element or Attribute           |
| ---------------------------- | ----------------------------------------------------- | ---------------------------------------- |
| CHANNEL_FILTER               | `ChannelProcessingFilter`                             | `http/intercept-url@requires-channel`    |
| SECURITY_CONTEXT_FILTER      | `SecurityContextPersistenceFilter`                    | `http`                                   |
| CONCURRENT_SESSION_FILTER    | `ConcurrentSessionFilter`                             | `session-management/concurrency-control` |
| HEADERS_FILTER               | `HeaderWriterFilter`                                  | `http/headers`                           |
| CSRF_FILTER                  | `CsrfFilter`                                          | `http/csrf`                              |
| LOGOUT_FILTER                | `LogoutFilter`                                        | `http/logout`                            |
| X509_FILTER                  | `X509AuthenticationFilter`                            | `http/x509`                              |
| PRE_AUTH_FILTER              | `AbstractPreAuthenticatedProcessingFilter` Subclasses | N/A                                      |
| CAS_FILTER                   | `CasAuthenticationFilter`                             | N/A                                      |
| FORM_LOGIN_FILTER            | `UsernamePasswordAuthenticationFilter`                | `http/form-login`                        |
| BASIC_AUTH_FILTER            | `BasicAuthenticationFilter`                           | `http/http-basic`                        |
| SERVLET_API_SUPPORT_FILTER   | `SecurityContextHolderAwareRequestFilter`             | `http/@servlet-api-provision`            |
| JAAS_API_SUPPORT_FILTER      | `JaasApiIntegrationFilter`                            | `http/@jaas-api-provision`               |
| REMEMBER_ME_FILTER           | `RememberMeAuthenticationFilter`                      | `http/remember-me`                       |
| ANONYMOUS_FILTER             | `AnonymousAuthenticationFilter`                       | `http/anonymous`                         |
| SESSION_MANAGEMENT_FILTER    | `SessionManagementFilter`                             | `session-management`                     |
| EXCEPTION_TRANSLATION_FILTER | `ExceptionTranslationFilter`                          | `http`                                   |
| FILTER_SECURITY_INTERCEPTOR  | `FilterSecurityInterceptor`                           | `http`                                   |
| SWITCH_USER_FILTER           | `SwitchUserFilter`                                    | N/A                                      |

可以添加自己的过滤器，使用`custom-filter`元素和上面这些名字之一来指定自定义过滤器出现的位置。

```xml
<http>
	<custom-filter position="FORM_LOGIN_FILTER" ref="myFilter" />
</http>

<beans:bean id="myFilter" class="com.mycompany.MySpecialAuthenticationFilter"/>
```

## 方法安全

可以使用`intercept-methods`元素来修饰bean的定义在单个bean上应用安全。也可以跨越整个服务层使用AspectJ风格切入点保护多个bean。

# 源码分析

## WEB项目

### Spring Boot

> 在接收到请求后，WEB容器使用 `org.apache.catalina.core.ApplicationFilterFactory#createFilterChain`生成请求的过滤器链。

过滤器的注册细节如下：

1. 注册DelegatingFilterProxy到Servlet容器

   [SecurityFilterAutoConfiguration](org.springframework.boot.autoconfigure.security.servlet.SecurityFilterAutoConfiguration)通过方法`securityFilterChainRegistration`注册一个过滤器`springSecurityFilterChain`到Servlet容器，它会匹配Spring容器中的同名Bean。

2. 注册真实的Filter类到Spring容器

   [WebSecurityConfiguration](`org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration)通过方法`springSecurityFilterChain`，注册真实的过滤器`springSecurityFilterChain`到Spring容器。

   这个过滤器的实现类为`org.springframework.security.web.FilterChainProxy`。

   `FilterChainProxy`内部包含一个filterChains字段，它是一个[DefaultSecurityFilterChain](org.springframework.security.web.DefaultSecurityFilterChain)（对应一个HttpSecurity）列表。

   每个`DefaultSecurityFilterChain`包含一个[RequestMatcher](`org.springframework.security.web.util.matcher.RequestMatcher`)和一个[Filter](javax.servlet.Filter)列表。根据配置顺序，第一个满足`RequestMatcher`的`DefaultSecurityFilterChain`才会生效。这个过滤器列表中包含的过滤器有如下：

这个过滤器中包含了完成Spring Security功能的拦截器（按照顺序，完整过滤器列表及顺序见附录部分-`FilterComparator`，添加默认Filter的操作在[WebSecurityConfigurerAdapter](org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter)的`getHttp`方法中完成。）：

* `org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter`

  > `OncePerRequestFilter`

  用于与AsyncManager集成。

  **处理所有请求**。

* `org.springframework.security.web.context.SecurityContextPersistenceFilter` 

  > `GenericFilterBean`

  用于将安全上下文注册到`org.springframework.security.core.context.SecurityContextHolder`，使之后的过滤器和controller方法都能访问到其保存的信息。在认证成功后，它会把认证信息存储到`SecurityContextRepository`中，后续每次访问时，它使用当前会话中的信息把认证信息取出来，完成认证跟踪。

  **处理所有请求**。

* `org.springframework.security.web.header.HeaderWriterFilter`

  > `OncePerRequestFilter`

  用于包装response的代理对象，为response添加定制的header信息。

  **处理所有请求**。

* `org.springframework.security.web.csrf.CsrfFilter`

  > `OncePerRequestFilter`

  用于抵御CSRF攻击。

  **处理匹配`DefaultRequiresCsrfMatcher`请求**。

* `org.springframework.security.web.authentication.logout.LogoutFilter`

  > `GenericFilterBean`

  用于检查当前请求是否为注销请求，如果是，则注销用户，并完成成功后操作；否则，则直接略过。

  **处理匹配`logoutRequestMatcher`请求**。

* `org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter`

  > `AbstractAuthenticationProcessingFilter`

  > > `GenericFilterBean`

  处理form登录请求，从请求参数中取出用户名（`username`）和密码（`password`），验证登录。

  **处理匹配`requiresAuthenticationRequestMatcher`请求**。

* `org.springframework.security.web.savedrequest.RequestCacheAwareFilter`

  > `GenericFilterBean`

  用于使用缓存包装request对象。

  **处理所有请求**。

* `org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter`

  > `GenericFilterBean`

  用于生成融合Spring Security API的request包装器，这个包装器还实现了Servlet API的安全方法。

  **处理所有请求**。

* `org.springframework.security.web.authentication.AnonymousAuthenticationFilter`

  > `GenericFilterBean`

  用于如果当前SecurityContextHolder中没有认证信息，那么就以匿名方式登录。

  **处理所有请求**。

* `org.springframework.security.web.session.SessionManagementFilter`

  > `GenericFilterBean`

  用于管理同一账号的会话状态。如只允许单会话登录等。

  1. 检查此用户是否已有活动会话，如果有活动会话，那么检查当前请求是否属于此会话，如果属于则直接跳过；
  2. 如果不属于，则检查用户请求是否已认证，如果未认证，则进入授权检查逻辑；
  3. 如果已认证，则调用`SessionAuthenticationStrategy`执行与会话有关的行为，如判断**一个账号是否同存在了2个会话**。

  **处理所有请求**。

* `org.springframework.security.web.access.ExceptionTranslationFilter`

  > `GenericFilterBean`

  处理过滤器链中抛出的`AccessDeniedException`和`AuthenticationException`异常。当`AuthenticationException`或者在匿名情形下出现`AccessDeniedException`时，用户将被重定向到登录页面。其他的`AccessDeniedException`情形，将由AccessDeniedHandler来处理。

  **处理所有请求**。

* `org.springframework.security.web.access.intercept.FilterSecurityInterceptor`

  > `AbstractSecurityInterceptor`

  用于安全检查。这个拦截器必须包含一个安全原数据源，它的类型为`FilterInvocationSecurityMetadataSource`。

  **处理所有请求**。

#### 关键过滤器分析

##### UsernamePasswordAuthenticationFilter与FilterSecurityInterceptor

![关键security Filter](reference-5.0.8.RELEASE/core-filter-illustrate.pos.png "key filter")

1. 配置请求安全配置（如添加和删除安全过滤器），可以使用方法`com.demo.spring.security.five.db.config.WebSecurityConfig#configure(org.springframework.security.config.annotation.web.builders.HttpSecurity)`，针对参数HttpSecurity进行添加和删除过滤器的操作：
   * `addFilterAt(Filter filter, Class<? extends Filter> atFilter)`
   * `removeConfigurer(Class<C> clazz)`
2. 配置认证管理器，可以使用方法`com.demo.spring.security.five.db.config.WebSecurityConfig#configure(org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder)`，AuthenticationManagerBuilder的`userDetailsService(UserDetailsService userDetailsService)`可以替换使用的UserDetailsService服务。

### PasswordEncoder

`org.springframework.security.crypto.factory.PasswordEncoderFactories#createDelegatingPasswordEncoder`这里使用代理类引入了内置的`PasswordEncoder`。

## Spring Security Oauth2

### AuthroizationServer

#### EnableAuthorizationServer

`org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer`

这个注解导入了2个配置：

* `org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerEndpointsConfiguration`

  配置所有的oauth终端。

  1. 注入了2个属性：`ClientDetailsService`和`List<AuthorizationServerConfigurer>`
  2. 在初始化方法中，使用`List<AuthorizationServerConfigurer>`的每一个元素配置endpoints配置器，并把`ClientDetailsService`的实例传递给endpoints配置器。

* `org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerSecurityConfiguration`

#### Oauth2 token请求方式

`org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor#extractToken`

1. 名称为`Authorization`的请求头。
2. 名称为`access_token`的请求参数。

#### 客户端验证

`org.springframework.security.oauth2.provider.client.ClientCredentialsTokenEndpointFilter`

#### token生成

`org.springframework.security.oauth2.provider.endpoint.TokenEndpoint#postAccessToken`

这部分包含了用户认证。

`org.springframework.security.oauth2.provider.CompositeTokenGranter`

`org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter`

`org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter`

`org.springframework.security.oauth2.provider.implicit.ImplicitTokenGranter`

`org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter`

`org.springframework.security.oauth2.provider.code.AuthorizationCodeTokenGranter`

![Token Granter](reference-5.0.8.RELEASE/token-granter-uml.png)

Token最后的生成都是在`org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices`的默认实现`org.springframework.security.oauth2.provider.token.DefaultTokenServices`中完成的。

##### token生成参数的配置

比如token有效期等。

`com.demo.authorization.server.service.DefaultClientDetailsServiceImpl`集成自`org.springframework.security.oauth2.provider.ClientDetailsService`

`com.demo.authorization.server.domain.DefaultClientDetails`继承自`org.springframework.security.oauth2.provider.ClientDetails`



#### 关键过滤器

* ClientCredentialsTokenEndpointFilter

  完成client认证`org.springframework.security.oauth2.provider.client.ClientCredentialsTokenEndpointFilter`



### ResourceServer

#### EnableResourceServer

`org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer`

#### 关键过滤器

* OAuth2AuthenticationProcessingFilter

  完成Token验证

  `org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter`





# 附录

## 其他中文版文档
[Spring Cloud中文网](https://springcloud.cc/spring-security-zhcn.html)


## 安全数据库方案

### 用户

```sql
create table users(
	username varchar_ignorecase(50) not null primary key,
	password varchar_ignorecase(50) not null,
	enabled boolean not null
);

create table authorities (
	username varchar_ignorecase(50) not null,
	authority varchar_ignorecase(50) not null,
	constraint fk_authorities_users foreign key(username) references users(username)
);
create unique index ix_auth_username on authorities (username,authority);
```

### 组权限

```sql
create table groups (
	id bigint generated by default as identity(start with 0) primary key,
	group_name varchar_ignorecase(50) not null
);

create table group_authorities (
	group_id bigint not null,
	authority varchar(50) not null,
	constraint fk_group_authorities_group foreign key(group_id) references groups(id)
);

create table group_members (
	id bigint generated by default as identity(start with 0) primary key,
	username varchar(50) not null,
	group_id bigint not null,
	constraint fk_group_members_group foreign key(group_id) references groups(id)
);
```

### 持久化登录（Remember-Me）

`JdbcTokenRepositoryImpl`

```
create table persistent_logins (
	username varchar(64) not null,
	series varchar(64) primary key,
	token varchar(64) not null,
	last_used timestamp not null
);
```


## FilterComparator

`org.springframework.security.config.annotation.web.builders.FilterComparator`

定义了预定义Filter的顺序，可以查看其默认构造器方法。